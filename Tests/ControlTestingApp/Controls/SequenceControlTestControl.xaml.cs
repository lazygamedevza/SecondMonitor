﻿using System.Windows.Controls;

namespace ControlTestingApp.Controls
{
    /// <summary>
    /// Interaction logic for SequenceControlTestControl.xaml
    /// </summary>
    public partial class SequenceControlTestControl : UserControl
    {
        public SequenceControlTestControl()
        {
            InitializeComponent();
        }
    }
}
