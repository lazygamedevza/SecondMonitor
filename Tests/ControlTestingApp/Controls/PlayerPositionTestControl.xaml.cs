﻿using System.Windows.Controls;

namespace ControlTestingApp.Controls
{
    /// <summary>
    /// Interaction logic for PlayerPositionTestControl.xaml
    /// </summary>
    public partial class PlayerPositionTestControl : UserControl
    {
        public PlayerPositionTestControl()
        {
            InitializeComponent();
        }
    }
}
