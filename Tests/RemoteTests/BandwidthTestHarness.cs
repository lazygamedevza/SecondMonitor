﻿namespace RemoteTests
{
    using System;
    using System.ComponentModel;
    using System.Threading.Tasks;
    using LiteNetLib;
    using Ninject;
    using NUnit.Framework;
    using SecondMonitor.Connector.Debug;
    using SecondMonitor.Foundation.Connectors;
    using SecondMonitor.PluginsConfiguration.Common.Controller;
    using SecondMonitor.PluginsConfiguration.Common.DataModel;
    using SecondMonitor.Remote.Application;
    using SecondMonitor.Remote.Application.Controllers;
    using SecondMonitor.Remote.Application.ViewModels;
    using SecondMonitor.Remote.Common;
    using SecondMonitor.Remote.Connector;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Settings;
    using SecondMonitor.ViewModels.Settings.ViewModel;

    [TestFixture]
    public class BandwidthTestHarness
    {
        private IKernel _serverKernel;
        private IKernel _connectorKernel;
        private IBroadCastServerController _broadCastServerController;
        private IRemoteClient _remoteClient;

        [Test]
        public async Task BasicMockedDataBandwidthTest()
        {
            IRawTelemetryConnector mockedConnector = new MockedConnector(null);
            mockedConnector.DataLoaded += MockedConnectorDataLoaded;
            mockedConnector.SessionStarted += MockedConnectorSessionStarted;

            if (!mockedConnector.TryConnect())
            {
                Assert.Fail("Mocked Connector has been disabled");
            }

            mockedConnector.StartConnectorLoop();
            var networkStats = _serverKernel.Get<INetworkStatsViewModelExtended>();

            await Task.Delay(TimeSpan.FromSeconds(30));

            Console.WriteLine($"Upload bytes per second: {networkStats.UploadBytesPerSecond}");
            Assert.That(networkStats.UploadBytesPerSecond, Is.GreaterThan(0L));
            Assert.That(networkStats.UploadBytesPerSecond, Is.LessThan(1024L * 250L));
        }

        [OneTimeSetUp]
        public async Task Bootstrap()
        {
            var viewModelsModule = new ViewModelsModule();
            var remoteCommonModule = new RemoteCommonModule();
            _serverKernel = new StandardKernel(viewModelsModule, remoteCommonModule, new RemoteApplicationModule());
            _connectorKernel = new StandardKernel(remoteCommonModule, new RemoteConnectorModule());
            _connectorKernel.Bind<Lazy<IPluginSettingsProvider>>()
                .ToMethod(_ => new Lazy<IPluginSettingsProvider>(() => _connectorKernel.Get<IPluginSettingsProvider>()));
            var serverPluginSettings = new PluginSettingsProvider();
            var connectorPluginSettings = new PluginSettingsProvider();

            _serverKernel.Bind<IPluginSettingsProvider>().ToConstant(serverPluginSettings);
            _connectorKernel.Bind<IPluginSettingsProvider>().ToConstant(connectorPluginSettings);

            _broadCastServerController = _serverKernel.Get<IBroadCastServerController>();
            _remoteClient = _connectorKernel.Get<IRemoteClient>();
            await _broadCastServerController.StartControllerAsync();
            await Task.Delay(TimeSpan.FromSeconds(5));

            if (!_remoteClient.TryConnect())
            {
                return;
            }

            _remoteClient.StartClientLoop();
        }

        private void MockedConnectorDataLoaded(object _, DataEventArgs args)
        {
            _broadCastServerController.SendRegularDataPackage(args.Data);
        }

        private void MockedConnectorSessionStarted(object _, DataEventArgs args)
        {
            _broadCastServerController.SendSessionStartedPackage(args.Data);
        }
    }

    internal class PluginSettingsProvider : IPluginSettingsProvider
    {
        public PluginsConfiguration PluginConfiguration { get; }

        public RemoteConfiguration RemoteConfiguration { get; } = new RemoteConfiguration()
        {
            HostAddress = NetUtils.GetLocalIp(LocalAddrType.IPv4),
            IsFindInLanEnabled = false,
            Port = 52642,
            BroadcastLimitSettings = new BroadcastLimitSettings()
            {
                IsEnabled = true,
                MinimumPackageInterval = 30,
                PlayerTimingPackageInterval = 200,
                OtherDriversTimingPackageInterval = 1000
            }
        };
        public F12019Configuration F12019Configuration { get; }
        public PCars2Configuration PCars2Configurations { get; }
        public AccConfiguration AccConfiguration { get; }
        
        public bool TryIsConnectorEnabled(string connectorName, out bool isEnabled)
        {
            throw new NotImplementedException();
        }

        public void SetConnectorEnabled(string connectorName, bool isConnectorEnabled)
        {
            throw new NotImplementedException();
        }

        public bool TryIsPluginEnabled(string pluginName, out bool isEnabled)
        {
            throw new NotImplementedException();
        }

        public void SetPluginEnabled(string pluginName, bool isPluginEnabled)
        {
            throw new NotImplementedException();
        }

        public void SaveConfiguration(PluginsConfiguration pluginsConfiguration)
        {
            throw new NotImplementedException();
        }
    }
}