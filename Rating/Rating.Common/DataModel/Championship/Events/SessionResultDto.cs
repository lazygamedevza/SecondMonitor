﻿namespace SecondMonitor.Rating.Common.DataModel.Championship.Events
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    [Serializable]
    public class SessionResultDto
    {
        public SessionResultDto()
        {
            DriverSessionResult = new List<DriverSessionResultDto>();
            ResultCreationTime = DateTime.UtcNow;
        }

        public List<DriverSessionResultDto> DriverSessionResult { get; set; }

        [XmlAttribute]
        public DateTime ResultCreationTime { get; set; }
    }
}