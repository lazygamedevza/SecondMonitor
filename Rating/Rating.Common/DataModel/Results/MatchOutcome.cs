﻿namespace SecondMonitor.Rating.Common.DataModel.Results
{
    public enum MatchOutcome
    {
        PlayerWon,
        PlayerLost,
    }
}