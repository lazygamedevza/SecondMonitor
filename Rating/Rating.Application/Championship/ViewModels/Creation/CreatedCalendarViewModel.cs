﻿namespace SecondMonitor.Rating.Application.Championship.ViewModels.Creation
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Input;
    using Calendar;
    using Common.Championship.Calendar;
    using Contracts.Commands;
    using Controller;
    using GongSolutions.Wpf.DragDrop;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Adorners;
    using SecondMonitor.ViewModels.Factory;

    public class CreatedCalendarViewModel : AbstractViewModel, IDropTarget
    {
        private readonly ICalendarEntryViewModelFactory _calendarEntryViewModelFactory;
        private readonly ITrackTemplateToSimTrackMapper _trackTemplateToSimTrackMapper;
        private readonly IDialogService _dialogService;
        private int _totalEvents;
        private int _randomEventsCount;
        private int _calendarYear;
        private ICommand _randomCalendarCommand;
        private DateTime _startOfCalendar;
        private DateTime _endOfCalendar;
        private bool _isCalendarDatesEnabled;
        private ObservableCollection<AbstractCalendarEntryViewModel> _calendarEntries;

        public CreatedCalendarViewModel(IViewModelFactory viewModelFactory, ICalendarEntryViewModelFactory calendarEntryViewModelFactory, ITrackTemplateToSimTrackMapper trackTemplateToSimTrackMapper, IDialogService dialogService)
        {
            _calendarEntryViewModelFactory = calendarEntryViewModelFactory;
            _trackTemplateToSimTrackMapper = trackTemplateToSimTrackMapper;
            _dialogService = dialogService;
            RandomEventsCount = 6;

            _startOfCalendar = new DateTime(DateTime.Now.Year, 3, 1);
            _endOfCalendar = new DateTime(DateTime.Now.Year, 11, 30);
            _calendarYear = DateTime.Now.Year;

            CalendarEntries = new ObservableCollection<AbstractCalendarEntryViewModel>();
        }

        public ObservableCollection<AbstractCalendarEntryViewModel> CalendarEntries
        {
            get => _calendarEntries;
            set => SetProperty(ref _calendarEntries, value);
        }

        public string SimulatorName { get; set; }

        public int TotalEvents
        {
            get => _totalEvents;
            set => SetProperty(ref _totalEvents, value);
        }

        public bool IsCalendarDatesEnabled
        {
            get => _isCalendarDatesEnabled;
            set => SetProperty(ref _isCalendarDatesEnabled, value);
        }

        public ICommand SelectPredefinedCalendarCommand
        {
            get;
            set;
        }

        public int CalendarYear
        {
            get => _calendarYear;
            set
            {
                SetProperty(ref _calendarYear, value);
                int yearDifference = _endOfCalendar.Year - _startOfCalendar.Year;
                _startOfCalendar = new DateTime(value, StartOfCalendar.Month, StartOfCalendar.Day);
                _endOfCalendar = new DateTime(value + yearDifference, EndOfCalendar.Month, EndOfCalendar.Day);
                NotifyPropertyChanged(nameof(StartOfCalendar));
                NotifyPropertyChanged(nameof(EndOfCalendar));
            }
        }

        public DateTime StartOfCalendar
        {
            get => _startOfCalendar;
            set
            {
                SetProperty(ref _startOfCalendar, value);
                _calendarYear = value.Year;
                NotifyPropertyChanged(nameof(CalendarYear));
            }
        }

        public DateTime EndOfCalendar
        {
            get => _endOfCalendar;
            set => SetProperty(ref _endOfCalendar, value);
        }

        public int RandomEventsCount
        {
            get => _randomEventsCount;
            set => SetProperty(ref _randomEventsCount, value);
        }

        public ICommand RandomCalendarCommand
        {
            get => _randomCalendarCommand;
            set => SetProperty(ref _randomCalendarCommand, value);
        }

        public void ApplyCalendarTemplate(CalendarTemplate calendarTemplate, bool useCalendarEventNames, bool autoReplaceKnownTracks)
        {
            CalendarEntries.Clear();
            foreach (EventTemplate calendarTemplateEvent in calendarTemplate.Events)
            {
                var newEntry = _calendarEntryViewModelFactory.Create(calendarTemplateEvent, SimulatorName, useCalendarEventNames, autoReplaceKnownTracks);
                newEntry.DeleteEntryCommand = new RelayCommand(() => DeleteCalendarEntry(newEntry));
                CalendarEntries.Add(newEntry);
            }

            RecalculateEventNumbers();
        }

        public void DragEnter(IDropInfo dropInfo)
        {
        }

        public void DragOver(IDropInfo dropInfo)
        {
            var target = (dropInfo.VisualTarget as FrameworkElement)?.DataContext;
            switch (target)
            {
                case CreatedCalendarViewModel _:
                    dropInfo.DropTargetAdorner = DropTargetAdorners.Insert;
                    dropInfo.Effects = DragDropEffects.Copy;
                    dropInfo.NotHandled = false;
                    return;
                case CalendarPlaceholderEntryViewModel _ when dropInfo.Data is ExistingTrackTemplateViewModel:
                    dropInfo.DropTargetAdorner = typeof(AllowDropAdorner);
                    dropInfo.Effects = DragDropEffects.Copy;
                    dropInfo.NotHandled = false;
                    return;
            }

            dropInfo.DropTargetAdorner = typeof(ForbidDropAdorner);
            dropInfo.Effects = DragDropEffects.None;
            dropInfo.NotHandled = false;
        }

        public void DragLeave(IDropInfo dropInfo)
        {
        }

        public void Drop(IDropInfo dropInfo)
        {
            object target = (dropInfo.VisualTarget as FrameworkElement)?.DataContext;
            if (dropInfo.Data is AbstractCalendarEntryViewModel abstractCalendarEntry && ReferenceEquals(target, this))
            {
                MoveCalendarEntry(abstractCalendarEntry, dropInfo.InsertIndex);
                return;
            }

            if (target is CalendarPlaceholderEntryViewModel calendarPlaceholder && dropInfo.Data is ExistingTrackTemplateViewModel existingTrackTemplateViewModel)
            {
                ReplacePlaceHolder(calendarPlaceholder, existingTrackTemplateViewModel);
                return;
            }

            if (dropInfo.Data is AbstractTrackTemplateViewModel trackTemplate && ReferenceEquals(target, this))
            {
                CreateEntry(trackTemplate, dropInfo.InsertIndex);
            }
        }

        public void ClearCalendar()
        {
            CalendarEntries.Clear();
        }

        public void AppendNewEntry(AbstractTrackTemplateViewModel trackTemplate)
        {
            CreateEntry(trackTemplate, CalendarEntries.Count);
        }

        private void ReplacePlaceHolder(CalendarPlaceholderEntryViewModel calendarPlaceholder, ExistingTrackTemplateViewModel existingTrackTemplateViewModel)
        {
            int index = _calendarEntries.IndexOf(calendarPlaceholder);
            _calendarEntries.Remove(calendarPlaceholder);
            CreateEntry(existingTrackTemplateViewModel, index, calendarPlaceholder.CustomEventName);
            if (_dialogService.ShowYesNoDialog("Always Replace", $"Do you want to always replace \n'{calendarPlaceholder.TrackName}' with \n'{existingTrackTemplateViewModel.TrackName}'?"))
            {
                _trackTemplateToSimTrackMapper.RegisterSimulatorTrackName(SimulatorName, calendarPlaceholder.TrackName, existingTrackTemplateViewModel.TrackName);
            }
        }

        private void DeleteCalendarEntry(AbstractCalendarEntryViewModel entryToDelete)
        {
            CalendarEntries.Remove(entryToDelete);
            RecalculateEventNumbers();
        }

        private void RecalculateEventNumbers()
        {
            TotalEvents = CalendarEntries.Count;
            for (int i = 0; i < CalendarEntries.Count; i++)
            {
                CalendarEntries[i].EventNumber = i + 1;
                CalendarEntries[i].OriginalEventName = "Round " + (i + 1);
            }
        }

        private void CreateEntry(AbstractTrackTemplateViewModel trackTemplate, int insertionIndex, string customEventName = "")
        {
            var newEntry = _calendarEntryViewModelFactory.Create(trackTemplate);
            newEntry.DeleteEntryCommand = new RelayCommand(() => DeleteCalendarEntry(newEntry));
            newEntry.CustomEventName = customEventName;
            CalendarEntries.Insert(insertionIndex, newEntry);
            RecalculateEventNumbers();
        }

        private void MoveCalendarEntry(AbstractCalendarEntryViewModel entry, int insertPosition)
        {
            int oldIndex = CalendarEntries.IndexOf(entry);
            if (oldIndex < insertPosition)
            {
                CalendarEntries.Move(oldIndex, Math.Max(0, insertPosition - 1));
            }
            else
            {
                CalendarEntries.Move(oldIndex, Math.Min(CalendarEntries.Count - 1, insertPosition));
            }

            RecalculateEventNumbers();
        }
    }
}