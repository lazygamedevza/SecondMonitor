﻿namespace SecondMonitor.Rating.Application.Championship.ViewModels.Events
{
    using Common.DataModel.Championship;
    using Common.DataModel.Championship.Events;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Factory;

    public class SessionResultWithTitleViewModel : AbstractViewModel<(ChampionshipDto Championship, EventDto EventDto, SessionDto SessionDto)>
    {
        public SessionResultWithTitleViewModel(IViewModelFactory viewModelFactory)
        {
            EventTitleViewModel = viewModelFactory.Create<EventTitleViewModel>();
            SessionResultViewModel = viewModelFactory.Create<SessionResultViewModel>();
        }

        public EventTitleViewModel EventTitleViewModel { get; }
        public SessionResultViewModel SessionResultViewModel { get; }
        protected override void ApplyModel((ChampionshipDto Championship, EventDto EventDto, SessionDto SessionDto) model)
        {
            var (championship, eventDto, sessionDto) = model;
            EventTitleViewModel.FromModel(model);
            SessionResultViewModel.FromModel(model.SessionDto.SessionResult);
        }

        public override (ChampionshipDto Championship, EventDto EventDto, SessionDto SessionDto) SaveToNewModel()
        {
            throw new System.NotImplementedException();
        }
    }
}