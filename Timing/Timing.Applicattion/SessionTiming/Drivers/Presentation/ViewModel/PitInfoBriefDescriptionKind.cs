﻿namespace SecondMonitor.Timing.Application.SessionTiming.Drivers.Presentation.ViewModel
{
    public enum PitInfoBriefDescriptionKind
    {
        None,
        PitStopFasterThanPlayer,
        PitStopSlowerThanPlayer,
        TyresOlderThanPlayer,
        TyresYoungerThanPlayers,
        PitEntry,
        InPits,
        PitExit
    }
}