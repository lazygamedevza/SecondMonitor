﻿namespace SecondMonitor.Timing.Application.ViewModel
{
    public enum TimingDataViewModelResetModeEnum
    {
        NoReset,

        Manual,

        Automatic
    }
}