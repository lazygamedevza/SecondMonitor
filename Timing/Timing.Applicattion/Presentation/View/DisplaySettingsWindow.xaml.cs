﻿namespace SecondMonitor.Timing.Application.Presentation.View
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for DisplaySettingsWindow.xaml
    /// </summary>
    public partial class DisplaySettingsWindow : Window
    {
        public DisplaySettingsWindow()
        {
            InitializeComponent();
        }
    }
}
