﻿namespace SecondMonitor.Timing.Common.SessionTiming
{
    public interface ISessionInfoProvider
    {
        ISessionInfo SessionInfo { get; }
    }
}