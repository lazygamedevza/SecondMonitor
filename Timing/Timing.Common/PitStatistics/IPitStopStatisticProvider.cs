﻿namespace SecondMonitor.Timing.Common.PitStatistics
{
    public interface IPitStopStatisticProvider
    {
        PitStatistics GetCurrentStatisticsPlayerClass();
    }
}