﻿namespace SecondMonitor.PluginManager.Core
{
    using System.Threading.Tasks;
    using Foundation.Connectors;

    public interface ISecondMonitorPlugin
    {
        PluginsManager PluginManager
        {
            get;
            set;
        }

        bool IsDaemon
        {
            get;
        }

        string PluginName { get; }
        bool IsEnabledByDefault { get; }

        Task RunPlugin();

        void OnSessionStarted(object sender, DataEventArgs e);

        void OnDataLoaded(object sender, DataEventArgs e);
        void OnDisplayMessage(object sender, MessageArgs e);
    }
}
