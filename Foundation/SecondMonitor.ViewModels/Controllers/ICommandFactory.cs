﻿namespace SecondMonitor.ViewModels.Controllers
{
    using System;
    using System.Windows.Input;

    public interface ICommandFactory
    {
        ICommand Create(Action operation);
    }
}