﻿namespace SecondMonitor.ViewModels.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using Ninject;
    using Ninject.Syntax;

    public class ChildControllerFactory : IChildControllerFactory
    {
        private readonly IResolutionRoot _resolutionRoot;

        public ChildControllerFactory(IResolutionRoot resolutionRoot)
        {
            _resolutionRoot = resolutionRoot;
        }

        public List<T> CreateAll<T, TParent>(TParent parentInstance) where T : IChildController<TParent> where TParent : IController
        {
            var childControllers = _resolutionRoot.GetAll<T>().ToList();
            childControllers.ForEach(x => x.ParentController = parentInstance);
            return childControllers;
        }

        public T Create<T, TParent>(TParent parentInstance) where T : IChildController<TParent> where TParent : IController
        {
            var childController = _resolutionRoot.Get<T>();
            childController.ParentController = parentInstance;
            return childController;
        }
    }
}