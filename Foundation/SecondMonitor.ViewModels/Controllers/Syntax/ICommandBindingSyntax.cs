﻿namespace SecondMonitor.ViewModels.Controllers.Syntax
{
    using System;
    using Contracts.Fluent;
    using Factory;

    public interface ICommandBindingSyntax<TViewModel> : IFluentSyntax where TViewModel : IViewModel
    {
        IViewModelBindingSyntax<TViewModel> To(Action action);

        void ExecuteCommandBinding(IViewModel viewModel, IViewModelFactory viewModelFactory);
    }
}