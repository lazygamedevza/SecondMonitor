﻿namespace SecondMonitor.ViewModels.Dialogs
{
    public class YesNoDialogViewModel : AbstractDialogViewModel
    {
        public string Title { get; set; }
        public string Text { get; set; }
    }
}