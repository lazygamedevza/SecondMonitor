﻿namespace SecondMonitor.ViewModels.CarStatus.FuelStatus
{
    using System;
    using DataModel.BasicProperties;
    using DataModel.Summary;

    public class FuelCalculatorViewModel : AbstractViewModel
    {
        private FuelConsumptionInfo _fuelConsumption;
        private int _requiredLaps;
        private int _requiredMinutes;
        private double _lapsDistance;
        private int _extraFuel;
        private Volume _requiredFuel;

        public FuelConsumptionInfo FuelConsumption
        {
            get => _fuelConsumption;
            set
            {
                SetProperty(ref _fuelConsumption, value);
                CalculateRequiredFuel();
            }
        }

        public int RequiredLaps
        {
            get => _requiredLaps;
            set
            {
                SetProperty(ref _requiredLaps, value);
                CalculateRequiredFuel();
            }
        }

        public int RequiredMinutes
        {
            get => _requiredMinutes;
            set
            {
                SetProperty(ref _requiredMinutes, value);
                CalculateRequiredFuel();
            }
        }

        public double LapDistance
        {
            get => _lapsDistance;
            set
            {
                SetProperty(ref _lapsDistance, value);
                CalculateRequiredFuel();
            }
        }

        public int ExtraFuel
        {
            get => _extraFuel;
            set
            {
                SetProperty(ref _extraFuel, value);
                CalculateRequiredFuel();
            }
        }

        public Volume RequiredFuel
        {
            get => _requiredFuel;
            set => SetProperty(ref _requiredFuel, value);
        }

        private void CalculateRequiredFuel()
        {
            if (FuelConsumption == null)
            {
                return;
            }

            double coefficient = 1 + (_extraFuel / 100.0);

            RequiredFuelCalculator calculator = new RequiredFuelCalculator(FuelConsumption);
            RequiredFuel = calculator.GetRequiredFuel(TimeSpan.FromMinutes(RequiredMinutes),
                Distance.FromMeters(LapDistance * RequiredLaps), coefficient);
        }
    }
}
