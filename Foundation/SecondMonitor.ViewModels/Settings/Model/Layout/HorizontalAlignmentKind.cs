﻿namespace SecondMonitor.ViewModels.Settings.Model.Layout
{
    public enum HorizontalAlignmentKind
    {
        Center,
        Left,
        Right,
        Stretch
    }
}