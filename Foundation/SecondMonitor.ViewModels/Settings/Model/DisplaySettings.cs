﻿namespace SecondMonitor.ViewModels.Settings.Model
{
    using System;
    using DataModel.BasicProperties;
    using DataModel.BasicProperties.Units;
    using ViewModel;

    [Serializable]
    public class DisplaySettings
    {
        public DisplaySettings()
        {
        }

        public TemperatureUnits TemperatureUnits { get; set; } = TemperatureUnits.Celsius;

        public PressureUnits PressureUnits { get; set; } = PressureUnits.Kpa;

        public VolumeUnits VolumeUnits { get; set; } = VolumeUnits.Liters;

        public VelocityUnits VelocityUnits { get; set; } = VelocityUnits.Kph;

        public ForceUnits ForceUnits { get; set; } = ForceUnits.Newtons;

        public AngleUnits AngleUnits { get; set; } = AngleUnits.Degrees;

        public MultiClassDisplayKind MultiClassDisplayKind { get; set; } = MultiClassDisplayKind.ClassFirst;

        public FuelCalculationScope FuelCalculationScope { get; set; } = FuelCalculationScope.Lap;

        public TorqueUnits TorqueUnits { get; set; } = TorqueUnits.Nm;

        public PowerUnits PowerUnits { get; set; } = PowerUnits.KW;

        public PitStopTimeDisplayKind PitStopTimeDisplayKind { get; set; } = PitStopTimeDisplayKind.Both;

        public bool ShowPitStopTimeRelative { get; set; } = true;

        public int PaceLaps { get; set; } = 3;

        public int RefreshRate { get; set; } = 300;

        public bool ScrollToPlayer { get; set; } = true;

        public bool AnimateDriversPosition { get; set; } = false;

        public bool AnimateDeltaTimes { get; set; } = false;

        public bool IsGapVisualizationEnabled { get; set; } = false;

        public double MinimalGapForVisualization { get; set; } = 2;

        public double GapHeightForOneSecond { get; set; } = 25;

        public double MaximumGapHeight { get; set; } = 150;

        public bool EnablePedalInformation { get; set; } = true;

        public bool EnableTemperatureInformation { get; set; } = true;

        public bool EnableNonTemperatureInformation { get; set; } = true;

        public SessionOptions PracticeOptions { get; set; } = new() { OrderingDisplayMode = DriverOrderKind.Absolute, TimesDisplayMode = DisplayModeEnum.Absolute, SessionName = "Practice" };

        public SessionOptions QualificationOptions { get; set; } = new() { OrderingDisplayMode = DriverOrderKind.Absolute, TimesDisplayMode = DisplayModeEnum.Absolute, SessionName = "Quali" };

        public SessionOptions RaceOptions { get; set; } = new() { OrderingDisplayMode = DriverOrderKind.Relative, TimesDisplayMode = DisplayModeEnum.Relative, SessionName = "Race" };

        public ReportingSettings ReportingSettings { get; set; } = new();

        public MapDisplaySettings MapDisplaySettings { get; set; } = new();

        public TelemetrySettings TelemetrySettings { get; set; } = new();

        public WindowLocationSetting WindowLocationSetting { get; set; }

        public WindowLocationSetting MapWindowLocationSettings { get; set; }

        public RatingSettings RatingSettings { get; set; } = new();

        public PitBoardSettings PitBoardSettings { get; set; } = new();

        public TrackRecordsSettings TrackRecordsSettings { get; set; } = new();

        public bool EnableCamberVisualization { get; set; } = false;

        public string CustomResourcesPath { get; set; } = string.Empty;

        public int DriversUpdatedPerTick { get; set; } = 10;

        public bool IsHwAccelerationEnabled { get; set; } = false;

        public LayoutSettings LayoutSettings { get; set; } = new();

        public bool IsForceSingeClassEnabled { get; set; } = false;

        public PitEstimationSettings PitEstimationSettings { get; set; } = new();

        public int MaximumExtraFuel { get; set; } = 15;

        public int MinimumExtraFuel { get; set; } = 5;

        public int RacesToTrainFuel { get; set; } = 5;

        public int ExtraRaceLaps { get; set; } = 1;
        public int ExtraRaceMinutes { get; set; } = 0;
        public int ExtraContingencyFuel { get; set; } = 5;
    }
}
