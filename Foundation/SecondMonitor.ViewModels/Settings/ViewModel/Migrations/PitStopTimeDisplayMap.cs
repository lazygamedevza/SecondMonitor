﻿namespace SecondMonitor.ViewModels.Settings.ViewModel.Migrations
{
    using Contracts;
    using DataModel.BasicProperties;

    public class PitStopTimeDisplayMap : AbstractHumanReadableMap<PitStopTimeDisplayKind>
    {
        public PitStopTimeDisplayMap()
        {
            Translations.Add(PitStopTimeDisplayKind.FullStop, "Entry to Exit");
            Translations.Add(PitStopTimeDisplayKind.PitStall, "In Pit Stall");
            Translations.Add(PitStopTimeDisplayKind.Both, "Combined");
        }
    }
}