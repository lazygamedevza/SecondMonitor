﻿namespace SecondMonitor.ViewModels.Settings.ViewModel.Migrations
{
    using System.Linq;
    using DataGrid;
    using DataModel.Extensions;
    using Model;

    public class ClassColumnNameBugMigration : ISettingMigration
    {
        public void MigrateUp(DisplaySettings displaySettings)
        {
            displaySettings.PracticeOptions.ColumnsSettings.Columns.OfType<TextColumnDescriptor>().Where(x => x.Name == "Class").ForEach(x => x.BindingPropertyName = "CarClassName");
            displaySettings.QualificationOptions.ColumnsSettings.Columns.OfType<TextColumnDescriptor>().Where(x => x.Name == "Class").ForEach(x => x.BindingPropertyName = "CarClassName");
            displaySettings.RaceOptions.ColumnsSettings.Columns.OfType<TextColumnDescriptor>().Where(x => x.Name == "Class").ForEach(x => x.BindingPropertyName = "CarClassName");
        }
    }
}