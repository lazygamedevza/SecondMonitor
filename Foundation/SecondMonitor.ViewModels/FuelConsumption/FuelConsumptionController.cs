﻿namespace SecondMonitor.ViewModels.FuelConsumption
{
    using System;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using CarStatus;
    using CarStatus.FuelStatus;
    using Controllers;
    using DataModel.BasicProperties;
    using DataModel.Extensions;
    using DataModel.Snapshot;
    using DataModel.Summary.FuelConsumption;
    using Factory;
    using NLog;
    using SessionEvents;
    using Settings;
    using Settings.ViewModel;

    public class FuelConsumptionController : AbstractController, IController, IFuelPredictionProvider
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static readonly TimeSpan MinimumSessionLength = TimeSpan.FromMinutes(2);
        private readonly FuelConsumptionRepository _fuelConsumptionRepository;
        private readonly Lazy<OverallFuelConsumptionHistory> _overallFuelHistoryLazy;
        private readonly ISessionEventProvider _sessionEventProvider;
        private readonly FuelPlannerViewModel _fuelPlannerViewModel;
        private readonly DisplaySettingsViewModel _displaySettingsViewModel;
        private bool _isFuelCalculatorShown;
        private SimulatorDataSet _lastDataSet;
        private bool _autoOpened;

        public FuelConsumptionController(FuelConsumptionRepository fuelConsumptionRepository, IPaceProvider paceProvider, ISessionEventProvider sessionEventProvider, ISettingsProvider settingsProvider, IViewModelFactory viewModelFactory) : base(viewModelFactory)
        {
            _displaySettingsViewModel = settingsProvider.DisplaySettingsViewModel;
            _fuelConsumptionRepository = fuelConsumptionRepository;
            _sessionEventProvider = sessionEventProvider;
            _overallFuelHistoryLazy = new Lazy<OverallFuelConsumptionHistory>(LoadOverallFuelConsumptionHistory);
            _fuelPlannerViewModel = ViewModelFactory.Create<FuelPlannerViewModel>();
            Bind<FuelOverviewViewModel>().Command(x => x.ShowFuelCalculatorCommand).To(ShowFuelCalculator);
            Bind<FuelOverviewViewModel>().Command(x => x.HideFuelCalculatorCommand).To(HideFuelCalculator);
            FuelOverviewViewModel = ViewModelFactory.Set<SessionRemainingCalculator>().To(new SessionRemainingCalculator(paceProvider)).Create<FuelOverviewViewModel>();
            FuelOverviewViewModel.IsVisible = false;
        }

        public FuelOverviewViewModel FuelOverviewViewModel { get; }
        private OverallFuelConsumptionHistory OverallFuelConsumptionHistory => _overallFuelHistoryLazy.Value;

        private bool IsFuelCalculatorShown
        {
            get => _isFuelCalculatorShown;
            set
            {
                Logger.Info($"FuelCalculator visibility switched to {value}");
                _isFuelCalculatorShown = value;
                //FuelOverviewViewModel.IsVisible = !value;
                FuelOverviewViewModel.FuelPlannerViewModel.IsVisible = value;
            }
        }

        public override Task StartControllerAsync()
        {
            _sessionEventProvider.PlayerPropertiesChanged += SessionEventProviderOnPlayerPropertiesChanged; 
            return Task.CompletedTask;
        }

        public override Task StopControllerAsync()
        {
            _sessionEventProvider.PlayerPropertiesChanged -= SessionEventProviderOnPlayerPropertiesChanged;
            StoreCurrentSessionConsumption();
            if (_overallFuelHistoryLazy.IsValueCreated)
            {
                _fuelConsumptionRepository.Save(OverallFuelConsumptionHistory);
            }

            return Task.CompletedTask;
        }

        public void ApplyDataSet(SimulatorDataSet dataSet)
        {
            try
            {
                _lastDataSet = dataSet;
                FuelOverviewViewModel.ApplyDateSet(dataSet);
                if (dataSet.SessionInfo.SessionType == SessionType.Race && dataSet.SessionInfo.SessionPhase == SessionPhase.Countdown && dataSet.PlayerInfo?.Speed.InKph < 5 && !_autoOpened)
                {
                    AutoOpenFuelCalculator(dataSet);
                }

                if (_autoOpened && (dataSet.SessionInfo.SessionPhase != SessionPhase.Countdown || dataSet.SessionInfo.SessionType != SessionType.Race || dataSet.PlayerInfo?.Speed.InKph > 60))
                {
                    _autoOpened = false;
                    HideFuelCalculator();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Error while applying data set");
                throw;
            }
        }

        public void Reset()
        {
            try
            {
                StoreCurrentSessionConsumption();

                if (_overallFuelHistoryLazy.IsValueCreated)
                {
                    _fuelConsumptionRepository.Save(OverallFuelConsumptionHistory);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Error while reseting FuelConsumption Controller");
            }

            FuelOverviewViewModel.Reset();
        }

        private void AutoOpenFuelCalculator(SimulatorDataSet dataSet)
        {
            _autoOpened = true;
            ShowFuelCalculator();
            int extraFuel = CalculateExtraFuelValue();
            FuelOverviewViewModel.FuelPlannerViewModel.CalculatorForSelectedSession.ExtraFuel = extraFuel;

            if (!_sessionEventProvider.CurrentSimulatorSettings.IsSessionLengthAvailableBeforeStart)
            {
                FuelOverviewViewModel.FuelPlannerViewModel.CalculatorForSelectedSession.RequiredLaps = 1;
                return;
            }

            int totalMinutes = 0;
            int totalLaps = 1;
            switch (dataSet.SessionInfo.SessionLengthType)
            {
                case SessionLengthType.Na:
                    break;
                case SessionLengthType.Laps:
                    totalLaps += dataSet.SessionInfo.TotalNumberOfLaps;
                    break;
                case SessionLengthType.Time:
                    totalMinutes = (int)Math.Ceiling(dataSet.SessionInfo.SessionTimeRemaining / 60);
                    break;
                case SessionLengthType.TimeWithExtraLap:
                    totalMinutes = (int)Math.Ceiling(dataSet.SessionInfo.SessionTimeRemaining / 60);
                    totalLaps++;
                    break;
            }

            FuelOverviewViewModel.FuelPlannerViewModel.CalculatorForSelectedSession.RequiredLaps = totalLaps;
            FuelOverviewViewModel.FuelPlannerViewModel.CalculatorForSelectedSession.RequiredMinutes = totalMinutes;
        }

        private int CalculateExtraFuelValue()
        {
            int raceCount = FuelOverviewViewModel.FuelPlannerViewModel.Sessions.Count(x => x.IsCheckedVisible && x.IsChecked && x.OriginalModel.SessionKind == SessionType.Race);
            int maxExtraFuel = _displaySettingsViewModel.MaximumExtraFuel;
            int minExtraFuel = _displaySettingsViewModel.MinimumExtraFuel;
            int racesToMinimum = _displaySettingsViewModel.RacesToTrainFuel;

            if (maxExtraFuel <= minExtraFuel)
            {
                return maxExtraFuel;
            }

            int range = maxExtraFuel - minExtraFuel;
            double racesFraction = raceCount / ((double)racesToMinimum);

            int extraFuel = (int)(maxExtraFuel - (range * racesFraction));
            return extraFuel;
        }

        private void ShowFuelCalculator()
        {
            try
            {
                if (!Application.Current.Dispatcher.CheckAccess())
                {
                    Application.Current.Dispatcher.Invoke(ShowFuelCalculator);
                    return;
                }

                Logger.Info("Opening Fuel Calculator");

                if (_lastDataSet?.PlayerInfo == null)
                {
                    Logger.Info("Unable to open Fuel Calculator - No Player");
                    return;
                }

                var fuelConsumptionEntries = OverallFuelConsumptionHistory.GetTrackConsumptionHistoryEntries(_lastDataSet.Source, _sessionEventProvider.LastTrackFullName, _sessionEventProvider.CurrentCarName);
                var currentSessionEntry = CreateCurrentSessionFuelConsumptionDto();
                if (currentSessionEntry != null)
                {
                    fuelConsumptionEntries = new[] { currentSessionEntry }.Concat(fuelConsumptionEntries).ToList();
                }

                if (FuelOverviewViewModel.FuelPlannerViewModel != null)
                {
                    FuelOverviewViewModel.FuelPlannerViewModel.Sessions.CollectionChanged -= SessionsOnCollectionChanged;
                }

                _fuelPlannerViewModel.FromModel(fuelConsumptionEntries.ToList());
                FuelOverviewViewModel.FuelPlannerViewModel = _fuelPlannerViewModel;
                IsFuelCalculatorShown = true;

                _fuelPlannerViewModel.Sessions.CollectionChanged += SessionsOnCollectionChanged;
                Logger.Info("Fuel Calculator Opened");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Unable to open Fuel Calculator, Exception");
            }
        }

        private void SessionsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Remove)
            {
                return;
            }

            var removedInfo = e.OldItems.OfType<ISessionFuelConsumptionViewModel>().Select(x => x.OriginalModel);
            removedInfo.ForEach(x => OverallFuelConsumptionHistory.RemoveTrackConsumptionHistoryEntry(x));
        }

        private void HideFuelCalculator()
        {
            IsFuelCalculatorShown = false;
        }

        private void StoreCurrentSessionConsumption()
        {
            if (_lastDataSet?.PlayerInfo == null || FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ElapsedTime < MinimumSessionLength ||
                FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ConsumedFuel.InLiters < 5)
            {
                Logger.Info("Fuel Consumption - Current Session Discared");
                return;
            }

            OverallFuelConsumptionHistory.AddTrackConsumptionHistoryEntry(CreateCurrentSessionFuelConsumptionDto());
        }

        private SessionFuelConsumptionDto CreateCurrentSessionFuelConsumptionDto()
        {
            if (_lastDataSet?.PlayerInfo == null || FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ElapsedTime < MinimumSessionLength)
            {
                return null;
            }

            var currentSessionFuelConsumption = new SessionFuelConsumptionDto()
            {
                IsWetSession = FuelOverviewViewModel.IsWetSession,
                CarName = _sessionEventProvider.CurrentCarName,
                LapDistance = _lastDataSet.SessionInfo.TrackInfo.LayoutLength.InMeters,
                RecordDate = DateTime.Now,
                SessionKind = _lastDataSet.SessionInfo.SessionType,
                Simulator = _lastDataSet.Source,
                TrackFullName = _lastDataSet.SessionInfo.TrackInfo.TrackFullName,
                TraveledDistanceMeters = FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.TraveledDistance.InMeters,
                ConsumedFuel = FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ConsumedFuel,
                ElapsedSeconds = FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ElapsedTime.TotalSeconds,
            };
            return currentSessionFuelConsumption;
        }

        private void SessionEventProviderOnPlayerPropertiesChanged(object sender, DataSetArgs e)
        {
            Reset();
        }

        private OverallFuelConsumptionHistory LoadOverallFuelConsumptionHistory()
        {
            return _fuelConsumptionRepository.LoadRatingsOrCreateNew();
        }

        public TimeSpan GetRemainingFuelTime()
        {
            return FuelOverviewViewModel.TimeLeft;
        }

        public double GetLapsLeft()
        {
            return FuelOverviewViewModel.LapsLeft;
        }
    }
}