﻿namespace SecondMonitor.ViewModels.Layouts.Factory
{
    public interface IDefaultLayoutFactory
    {
        LayoutDescription CreateDefaultLayout();
    }
}