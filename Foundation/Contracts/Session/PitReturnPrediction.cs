﻿namespace SecondMonitor.Contracts.Session
{
    using System;
    using DataModel.BasicProperties;

    public class PitReturnPrediction
    {
        public PitReturnPrediction(int overallReturnPosition, int classReturnPosition, double returnLapDistance, double returnTotalDistance, Point3D returnWorldPos, TimeSpan pitTime)
        {
            OverallReturnPosition = overallReturnPosition;
            ClassReturnPosition = classReturnPosition;
            ReturnLapDistance = returnLapDistance;
            ReturnTotalDistance = returnTotalDistance;
            ReturnWorldPos = returnWorldPos;
            PitTime = pitTime;
        }

        public int OverallReturnPosition { get; }

        public int ClassReturnPosition { get; }

        public double ReturnLapDistance { get; }

        public double ReturnTotalDistance { get; }

        public Point3D ReturnWorldPos { get; }

        public TimeSpan PitTime { get; }
    }
}
