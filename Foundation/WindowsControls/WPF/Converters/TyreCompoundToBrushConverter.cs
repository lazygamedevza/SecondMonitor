﻿namespace SecondMonitor.WindowsControls.WPF.Converters
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    public class TyreCompoundToBrushConverter : IValueConverter
    {
        private static readonly ResourceDictionary ColorResourceDictionary = new ResourceDictionary
        {
            Source = new Uri(@"pack://application:,,,/WindowsControls;component/WPF/Colors.xaml", UriKind.RelativeOrAbsolute)
        };

        private static readonly SolidColorBrush UltraSoftBrush;
        private static readonly SolidColorBrush SuperSoftBrush;
        private static readonly SolidColorBrush SoftBrush;
        private static readonly SolidColorBrush MediumBrush;
        private static readonly SolidColorBrush HardBrush;
        private static readonly SolidColorBrush IntermediateBrush;
        private static readonly SolidColorBrush WetBrush;

        static TyreCompoundToBrushConverter()
        {
            UltraSoftBrush = (SolidColorBrush)ColorResourceDictionary["TyreUltraSoft"];
            SuperSoftBrush = (SolidColorBrush)ColorResourceDictionary["TyreSuperSoft"];
            SoftBrush = (SolidColorBrush)ColorResourceDictionary["TyreSoft"];
            MediumBrush = (SolidColorBrush)ColorResourceDictionary["TyreMedium"];
            HardBrush = (SolidColorBrush)ColorResourceDictionary["TyreHard"];
            IntermediateBrush = (SolidColorBrush)ColorResourceDictionary["TyreIntermediate"];
            WetBrush = (SolidColorBrush)ColorResourceDictionary["TyreWet"];
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string stringValue))
            {
                return Brushes.Transparent;
            }

            stringValue = stringValue.ToLower();
            if (stringValue.Contains("ultrasoft"))
            {
                return UltraSoftBrush;
            }

            if (stringValue.Contains("supersoft"))
            {
                return SuperSoftBrush;
            }

            if (stringValue.Contains("soft") || stringValue.Contains("option"))
            {
                return SoftBrush;
            }

            if (stringValue.Contains("medium"))
            {
                return MediumBrush;
            }

            if (stringValue.Contains("hard") || stringValue.Contains("prime"))
            {
                return HardBrush;
            }

            if (stringValue.Contains("inter"))
            {
                return IntermediateBrush;
            }

            if (stringValue.Contains("wet") || stringValue.Contains("rain"))
            {
                return WetBrush;
            }

            if (stringValue.Contains("slick"))
            {
                return MediumBrush;
            }

            return Brushes.Transparent;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}