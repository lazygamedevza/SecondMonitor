﻿namespace SecondMonitor.WindowsControls.WPF.Behaviors.VolumeBehavior
{
    using DataModel.BasicProperties;

    public class ForegroundByPressureBehavior : ForegroundByOptimalVolumeBehavior<Pressure>
    {
    }
}