﻿namespace SecondMonitor.DataModel
{
    using System;

    public static class SimulatorsNameMap
    {
        public const string NotConnected = "Not connected";

        public const string R3ESimName = "R3E";
        public const string ACSimName = "Assetto Corsa";
        public const string ACCSimName = "ACC";
        public const string PCars2SimName = "PCars 2";
        public const string AMS2SimName = "AMS 2";
        public const string AMSSimName = "AMS";
        public const string GTR2SimName = "GTR2";
        public const string RF2SimName = "RFactor 2";
        public const string PCarsSimName = "Pcars";
        public const string F12019 = "F1 2019";
        public const string F12020 = "F1 2020";
        public const string F12021 = "F1 2021";

        public static bool IsR3ESimulator(string simulatorName)
        {
            return simulatorName.Equals(R3ESimName, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsNotConnected(string simulatorName)
        {
            return simulatorName.Equals(NotConnected, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}