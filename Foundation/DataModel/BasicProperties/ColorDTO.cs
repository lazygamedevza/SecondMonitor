﻿namespace SecondMonitor.DataModel.BasicProperties
{
    using System;
    using System.Windows.Media;
    using System.Xml.Serialization;

    [Serializable]
    public sealed class ColorDto
    {
        public static readonly ColorDto RedColor = FromColor(Colors.Red);
        public static readonly ColorDto DarkBlueColor = FromColor(Colors.DarkBlue);
        public static readonly ColorDto OrangeColor = FromColor(Colors.Orange);
        public static readonly ColorDto GreenColor = FromColor(Colors.Green);
        public static readonly ColorDto LimeGreenColor = FromColor(Colors.LimeGreen);
        public static readonly ColorDto YellowColor = FromColor(Colors.Yellow);
        public static readonly ColorDto GreenYellowColor = FromColor(Colors.GreenYellow);
        public static readonly ColorDto WhiteColor = FromColor(Colors.White);
        public static readonly ColorDto BlackColor = FromColor(Colors.Black);
        public static readonly ColorDto SilverColor = ColorDto.FromHex("FF7F7F7F");

        public ColorDto(byte alpha, byte red, byte green, byte blue)
        {
            Alpha = alpha;
            Red = red;
            Green = green;
            Blue = blue;
        }

        public ColorDto()
        {
        }

        [XmlAttribute]
        public byte Alpha { get; set; }

        [XmlAttribute]
        public byte Red { get; set; }

        [XmlAttribute]
        public byte Green { get; set; }

        [XmlAttribute]
        public byte Blue { get; set; }

        public static ColorDto FromColor(Color color)
        {
            return new ColorDto()
            {
                Alpha = color.A,
                Blue = color.B,
                Green = color.G,
                Red = color.R,
            };
        }

        public static ColorDto FromHex(string hex)
        {
            hex = hex.Replace("#", string.Empty);
            return new ColorDto()
            {
                Alpha = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.AllowHexSpecifier),
                Red = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.AllowHexSpecifier),
                Green = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.AllowHexSpecifier),
                Blue = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.AllowHexSpecifier),
            };
        }

        public Color ToColor()
        {
            return Color.FromArgb(Alpha, Red, Green, Blue);
        }

        public SolidColorBrush ToSolidColorBrush()
        {
            return new SolidColorBrush(ToColor());
        }
    }
}