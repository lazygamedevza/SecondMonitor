﻿namespace SecondMonitor.DataModel.BasicProperties
{
    public enum HeadLightsStatus
    {
        Na,
        Off,
        NormalBeams,
        HighBeams,
    }
}