﻿namespace Plugins.Debug
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using global::Debug.Common;
    using ProtoBuf;
    using SecondMonitor.DataModel.Snapshot;
    using SecondMonitor.Foundation.Connectors;
    using SecondMonitor.PluginManager.Core;
    using SecondMonitor.ViewModels.Settings;

    public class RawTelemetryCapturePlugin: ISecondMonitorPlugin
    {
        private readonly ISettingsProvider _settingsProvider;
        
        private FileInfo _currentFile = null;

        public RawTelemetryCapturePlugin(ISettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
        }

        public PluginsManager PluginManager
        {
            get;
            set;
        }
        public bool IsDaemon => true;
        public string PluginName => "Raw Telemetry Capture";
        public bool IsEnabledByDefault => false;
        public Task RunPlugin()
        {
            return Task.CompletedTask;
        }

        public void OnDisplayMessage(object sender, MessageArgs e)
        {
        }

        public void OnDataLoaded(object sender, DataEventArgs e)
        {
            WriteToFile(e.Data);
        }

        public void OnSessionStarted(object sender, DataEventArgs e)
        {
            var tempFileName = Path.GetTempFileName();
            var tempFileInfo = new FileInfo(tempFileName);
            var secondMonitorTelemetryFileName = Path.Combine(tempFileInfo.Directory.FullName, $"SecondMonitor-Telemetry-{DateTime.Now.ToString("yyyyMMdd-HHmmsszz")}.smtmp");
            tempFileInfo.MoveTo(secondMonitorTelemetryFileName);
            WriteToFile(e.Data, tempFileInfo);
        }

        private void WriteToFile(SimulatorDataSet data, FileInfo newFile = null)
        {
            if (_currentFile == null && newFile == null)
            {
                return;
            }
            
            var packet = new DataPacket()
            {
                Source = data.Source,
                InputInfo = data.InputInfo,
                SessionInfo = data.SessionInfo,
                DriversInfo = data.DriversInfo,
                PlayerInfo = data.PlayerInfo,
                LeaderInfo = data.LeaderInfo,
                SimulatorSourceInfo = data.SimulatorSourceInfo
            };

            var fileInfoToUse = newFile ?? _currentFile;
            
            using (var file = fileInfoToUse.Open(FileMode.Append, FileAccess.Write))
            {
                if (newFile != null)
                {
                    // Write Logging interval
                    var header = new DataHeader()
                    {
                        UpdateInterval = _settingsProvider.DisplaySettingsViewModel.TelemetrySettingsViewModel.LoggingInterval
                    };
                    Serializer.SerializeWithLengthPrefix(file, header, PrefixStyle.Base128);
                    
                    _currentFile = newFile;
                }

                lock (_currentFile)
                {
                    Serializer.SerializeWithLengthPrefix(file, packet, PrefixStyle.Fixed32);
                }
            }
        }
    }
}