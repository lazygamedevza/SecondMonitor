﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Controllers.Synchronization.Graphs
{
    public enum DistanceRequestKind
    {
        ByReferenceLap,
        ForAllLaps
    }
}