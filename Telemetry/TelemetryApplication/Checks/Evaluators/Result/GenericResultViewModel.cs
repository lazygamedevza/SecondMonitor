﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Checks.Evaluators.Result
{
    using System;
    using System.Linq;
    using SecondMonitor.ViewModels;

    public class GenericResultViewModel : AbstractViewModel<EvaluationResult>, IEvaluationViewModel
    {
        public string EvaluatorName { get; set; }

        public EvaluationResult EvaluationResult { get; private set; }

        public EvaluationResultKind EvaluationResultKind
        {
            get;
            set;
        }

        protected override void ApplyModel(EvaluationResult model)
        {
            EvaluationResult = model;
            EvaluationResultKind = EvaluationResult.Items.Concat(EvaluationResult.Groups.SelectMany(x => x.Items)).Max(x => x.ItemResultKind);
        }

        public override EvaluationResult SaveToNewModel()
        {
            throw new NotSupportedException();
        }
    }
}