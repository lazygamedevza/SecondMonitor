﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Checks.Evaluators.Result
{
    public class EvaluationSeparatorItem : EvaluationResultItem
    {
        public EvaluationSeparatorItem() : base(string.Empty, string.Empty, EvaluationResultKind.Information)
        {
        }
    }
}