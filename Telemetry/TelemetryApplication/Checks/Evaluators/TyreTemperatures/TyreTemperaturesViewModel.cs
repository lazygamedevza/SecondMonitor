﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Checks.Evaluators.TyreTemperatures
{
    using Result;

    public class TyreTemperaturesViewModel : AbstractWheelEvaluationResultViewModel
    {
        public TyreTemperaturesViewModel() : base("Tyre Temps")
        {
        }
    }
}