﻿namespace SecondMonitor.TelemetryPresentation.Controls.SnapshotSection
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for SnapshotSectionControl.xaml
    /// </summary>
    public partial class SnapshotSectionControl : UserControl
    {
        public SnapshotSectionControl()
        {
            InitializeComponent();
        }
    }
}
