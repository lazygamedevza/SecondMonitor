﻿namespace SecondMonitor.Remote.Application
{
    using Controllers;
    using Ninject;
    using Ninject.Activation;
    using Ninject.Modules;
    using PluginsConfiguration.Common.Controller;
    using PluginsConfiguration.Common.DataModel;
    using ViewModels;

    public class RemoteApplicationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IServerOverviewViewModel>().To<ServerOverviewViewModel>().InSingletonScope();
            Bind<IClientViewModel>().To<ClientViewModel>();
            Bind<INetworkStatsViewModelExtended, INetworkStatsViewModel>().To<NetworkStatsViewModel>().InSingletonScope();

            Bind<IBroadCastServerController>().To<BroadCastServerController>();
        }
    }
}