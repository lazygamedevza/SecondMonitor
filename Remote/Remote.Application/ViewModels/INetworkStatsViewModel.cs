﻿namespace SecondMonitor.Remote.Application.ViewModels
{
    using LiteNetLib;
    using SecondMonitor.ViewModels;

    public interface INetworkStatsViewModel : IViewModel<NetStatistics>
    {
        string UploadDownloadSpeed { get; }
    }

    internal interface INetworkStatsViewModelExtended : INetworkStatsViewModel
    {
        long UploadBytesPerSecond { get; }
        long DownloadBytesPerSecond { get; }
    }
}
