﻿namespace SecondMonitor.Remote.Application.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Threading;
    using LiteNetLib;
    using SecondMonitor.Remote.Application.Extension;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Factory;

    public class ServerOverviewViewModel : AbstractViewModel, IServerOverviewViewModel
    {
        private readonly IViewModelFactory _viewModelFactory;
        private readonly Dictionary<string, IClientViewModel> _ipClientDictionary;
        private List<string> _availableIps;
        private bool _isRunning;
        private ObservableCollection<IClientViewModel> _connectedClients;
        private INetworkStatsViewModel _networkStatsViewModel;

        private double _throttleInput;
        private double _clutchInput;
        private double _brakeInput;
        private string _connectedSimulator;

        public ServerOverviewViewModel(IViewModelFactory viewModelFactory)
        {
            _viewModelFactory = viewModelFactory;
            _ipClientDictionary = new Dictionary<string, IClientViewModel>();
            ConnectedClients = new ObservableCollection<IClientViewModel>();
            NetworkStatsViewModel = _viewModelFactory.Create<INetworkStatsViewModel>();
        }

        public List<string> AvailableIps
        {
            get => _availableIps;
            set => SetProperty(ref _availableIps, value);
        }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public double ThrottleInput
        {
            get => _throttleInput;
            set => SetProperty(ref _throttleInput, value);
        }

        public double ClutchInput
        {
            get => _clutchInput;
            set => SetProperty(ref _clutchInput, value);
        }

        public double BrakeInput
        {
            get => _brakeInput;
            set => SetProperty(ref _brakeInput, value);
        }

        public string ConnectedSimulator
        {
            get => _connectedSimulator;
            set => SetProperty(ref _connectedSimulator, value);
        }

        public ObservableCollection<IClientViewModel> ConnectedClients
        {
            get => _connectedClients;
            private set => SetProperty(ref _connectedClients, value);
        }

        public INetworkStatsViewModel NetworkStatsViewModel
        {
            get => _networkStatsViewModel;
            private set => SetProperty(ref _networkStatsViewModel, value);
        }

        public void AddClient(NetPeer netPeer)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => AddClient(netPeer));
                return;
            }

            RemoveClient(netPeer);
            IClientViewModel newViewModel = _viewModelFactory.Create<IClientViewModel>();
            newViewModel.FromModel(netPeer);
            _connectedClients.Add(newViewModel);
            _ipClientDictionary[netPeer.GetIdentifier()] = newViewModel;
        }

        public void RemoveClient(NetPeer netPeer)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => RemoveClient(netPeer));
                return;
            }

            if (!_ipClientDictionary.ContainsKey(netPeer.GetIdentifier()))
            {
                return;
            }

            IClientViewModel removedViewModel = _ipClientDictionary[netPeer.GetIdentifier()];
            _ipClientDictionary.Remove(netPeer.GetIdentifier());
            _connectedClients.Remove(removedViewModel);
        }

        public void SampleStats(NetStatistics netStatistics)
        {
            if (!Dispatcher.CurrentDispatcher.CheckAccess())
            {
                Dispatcher.CurrentDispatcher.Invoke(() => SampleStats(netStatistics));
                return;
            }

            NetworkStatsViewModel.FromModel(netStatistics);
        }
    }
}