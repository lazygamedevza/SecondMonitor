﻿namespace SecondMonitor.Remote.Connector
{
    using System;
    using DataModel.Snapshot;

    public interface IRemoteClient
    {
        event EventHandler Disconnected;
        event EventHandler<SimulatorDataSet> SessionStarted;
        event EventHandler<SimulatorDataSet> DataLoaded;
        bool TryConnect();

        void StartClientLoop();
    }
}
