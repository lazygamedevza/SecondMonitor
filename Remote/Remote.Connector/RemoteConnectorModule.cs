﻿namespace SecondMonitor.Remote.Connector
{
    using Ninject;
    using Ninject.Activation;
    using Ninject.Modules;
    using PluginsConfiguration.Common.Controller;
    using PluginsConfiguration.Common.DataModel;

    public class RemoteConnectorModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IRemoteClient>().To<RemoteClient>();
        }
    }
}
