﻿namespace SecondMonitor.Remote.Common.Adapter
{
    using DataModel.Snapshot;
    using Model;

    public interface IDatagramPayloadPacker
    {
        bool IsMinimalPackageDelayPassed();
        DatagramPayload CreateHandshakeDatagramPayload();
        DatagramPayload CreateHearthBeatDatagramPayload();
        DatagramPayload CreateRegularDatagramPayload(SimulatorDataSet simulatorData);
        DatagramPayload CreateSessionStartedPayload(SimulatorDataSet simulatorData);
    }
}