﻿namespace SecondMonitor.Remote.Common.Model
{
    using DataModel.Snapshot;
    using ProtoBuf;

    [ProtoContract]
    public class SimulatorInfoPayload
    {
        public SimulatorInfoPayload()
        {
            SimulatorName = string.Empty;
            SimulatorSourceInfo = new SimulatorSourceInfo();
        }

        public SimulatorInfoPayload(string simulatorName, SimulatorSourceInfo simulatorSourceInfo)
        {
            SimulatorName = simulatorName;
            SimulatorSourceInfo = simulatorSourceInfo;
        }

        [ProtoMember(1, IsRequired = true)]
        public string SimulatorName { get; set; }

        [ProtoMember(2, IsRequired = true)]
        public SimulatorSourceInfo SimulatorSourceInfo { get; set; }
    }
}