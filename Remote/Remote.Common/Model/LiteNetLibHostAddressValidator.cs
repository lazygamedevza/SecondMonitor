﻿namespace SecondMonitor.Remote.Common.Model
{
    using System;
    using LiteNetLib;
    using ViewModels.PluginsSettings;

    public class LiteNetLibHostAddressValidator : IHostAddressValidator
    {
        public bool IsValidHostAddress(string value)
        {
            try
            {
                NetUtils.ResolveAddress(value);

                return true;
            }
            catch (ArgumentException)
            {
                return false;
            }
        }
    }
}