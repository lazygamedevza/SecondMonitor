﻿namespace SecondMonitor.F12020Connector.DataModels.Enums
{
    internal enum DriverResultKind
    {
        Na,
        Inactive,
        Active,
        Finished,
        Disqualified,
        NotClassified,
        Retired,
    }
}