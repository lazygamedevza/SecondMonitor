﻿namespace SecondMonitor.F12021Connector.DataModels.Enums
{
    public enum SessionKind
    {
        Na,
        Practice1,
        Practice2,
        Practice3,
        ShortPractice,
        Qualification1,
        Qualification2,
        Qualification3,
        ShortQualification,
        OnlineQualification,
        Race,
        Race2,
        Race3,
        TimeTrial
    }
}