namespace SecondMonitor.PCarsConnector.Enums
{
    public enum ETyres
    {
        TyreFrontLeft = 0,

        TyreFrontRight,

        TyreRearLeft,

        TyreRearRight,

        // --------------
        TyreMax
    }
}
